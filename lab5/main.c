#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

#define NOT_CANCELED 1
#define START_HANDLER 1

void cancellationHandler(void* args) {
    printf("Cancellation handler: child thread was canceled\n");
}

void* printLines(void* args) {
    pthread_cleanup_push(cancellationHandler, NULL);

            char* message = "Child Thread\n";
            size_t messageLength = strlen(message);

            while (NOT_CANCELED) {
                write(STDIN_FILENO, message, messageLength);
            }

    pthread_cleanup_pop(START_HANDLER);
}

int main(void) {
    int error;
    pthread_t childThread;

    error = pthread_create(&childThread, NULL, printLines, NULL);
    if (error) {
        fprintf(stderr, "pthread_create error: %s\n", strerror(error));
        return EXIT_FAILURE;
    }

    sleep(2);

    pthread_cancel(childThread);

    error = pthread_join(childThread, NULL);
    if (error) {
        fprintf(stderr, "pthread_join error: %s\n", strerror(error));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}