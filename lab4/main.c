#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

#define NOT_CANCELED 1
#define SLEEP_TIME 2

void* printLines(void* args) {
    char* message = "Child Thread\n";
    size_t messageLength = strlen(message);

    while (NOT_CANCELED) {
        write(STDIN_FILENO, message, messageLength);
    }
}

int main(void) {
    int error;
    pthread_t childThread;

    error = pthread_create(&childThread, NULL, printLines, NULL);
    if (error) {
        fprintf(stderr, "pthread_create error: %s\n", strerror(error));
        return EXIT_FAILURE;
    }

    sleep(SLEEP_TIME);

    pthread_cancel(childThread);

    error = pthread_join(childThread, NULL);
    if (error) {
        fprintf(stderr, "pthread_join error: %s\n", strerror(error));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}