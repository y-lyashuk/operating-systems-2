#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>

#define NO_OF_LINES 10

void printLines(char* message) {
    if (message == NULL) {
        fprintf(stderr, "error: null-pointer as an argument");
        return;
    }

    for (int i = 0; i < NO_OF_LINES; i++) {
        printf("%s: %d\n", message, i);
    }
}

void* bodyOfChildThread(void* args) {
    printLines("child");
    return NULL;
}

int main(int argc, char** argv) {
    pthread_t thread;

    int error = pthread_create(&thread, NULL, bodyOfChildThread, NULL);
    if (error) {
        fprintf(stderr, "pthread_create error: %s\n", strerror(error));
        return EXIT_FAILURE;
    }

    printLines("parent");

    pthread_exit(NULL);

    return EXIT_SUCCESS;
}