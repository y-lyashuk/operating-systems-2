#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

#define ITERATIONS_COUNT 200000

#define EXPECTED_AMOUNT_OF_ARGUMENTS 1
#define DECIMAL 10

typedef struct ThreadInfo {
    int start;
    int end;

    double result;
} ThreadInfo;

int initializeThreadInfo(ThreadInfo* infoPerThread, long threadsCount) {
    if (infoPerThread == NULL || threadsCount <= 0) {
        fprintf(stderr, "initializeThreadInfo: incorrect arguments\n");
        return EXIT_FAILURE;
    }

    const int iterationsPerThread = ITERATIONS_COUNT / threadsCount;
    const int remainder = ITERATIONS_COUNT % threadsCount;

    infoPerThread[0].start = 0;
    infoPerThread[0].end = iterationsPerThread + (remainder ? 1 : 0);

    for (int threadIdx = 1; threadIdx < threadsCount; threadIdx++) {
        infoPerThread[threadIdx].start = infoPerThread[threadIdx - 1].end;
        infoPerThread[threadIdx].end = infoPerThread[threadIdx].start + iterationsPerThread + ((threadIdx < remainder) ? 1 : 0);
    }

    return EXIT_SUCCESS;
}

void* calculatePartialSum(void* args) {
    if (args == NULL) {
        fprintf(stderr, "calculatePartialSum: NULL pointer has been passed\n");
        return NULL;
    }

    ThreadInfo* info = (ThreadInfo*) args;

    info->result = 0.0;
    for (int i = info->start; i < info->end; i++) {

        info->result += 1.0/(i*4.0 + 1.0);
        info->result -= 1.0/(i*4.0 + 3.0);
    }

    return &(info->result);
}

int calculatePi(double* pi, long desiredAmountOfThreads) {
    if (pi == NULL || desiredAmountOfThreads <= 0) {
        fprintf(stderr, "calculatePi: incorrect argument\n");
        return EXIT_FAILURE;
    }

    ThreadInfo* infoPerThread = (ThreadInfo*) malloc(sizeof(ThreadInfo) * desiredAmountOfThreads);
    if (infoPerThread == NULL) {
        perror("Could not allocate memory for infoPerThread array:");
        return EXIT_FAILURE;
    }

    initializeThreadInfo(infoPerThread, desiredAmountOfThreads);

    pthread_t* threads = (pthread_t*) malloc(sizeof(pthread_t) * desiredAmountOfThreads);
    if (threads == NULL) {
        perror("Could not allocate threads array:");

        free(infoPerThread);
        return EXIT_FAILURE;
    }

    int error, returnValue = EXIT_SUCCESS;
    long threadCount;

    for (threadCount = 0; threadCount < desiredAmountOfThreads; threadCount++) {
        error = pthread_create(&threads[threadCount], NULL, calculatePartialSum, &infoPerThread[threadCount]);
        if (error) {
            fprintf(stderr, "pthread_create error: %s\n", strerror(error));
            returnValue = EXIT_FAILURE;
            break;
        }
    }

    void* threadResult;
    *pi = 0.0;
    for (int i = 0; i < threadCount; i++) {
        error = pthread_join(threads[i], &threadResult);
        if (error) {
            fprintf(stderr, "pthread_join error: %s\n", strerror(error));
            returnValue = EXIT_FAILURE;
        }
        if (threadResult == NULL) {
            fprintf(stderr, "Thread %d returns null pointer\n", i);
            returnValue = EXIT_FAILURE;
        }

        *pi += *((double*)threadResult);
    }

    *pi *= 4;

    free(threads);
    free(infoPerThread);

    return returnValue;
}

int main(int argc, char** argv) {
    if (argc != EXPECTED_AMOUNT_OF_ARGUMENTS + 1) {
        fprintf(stderr, "One argument expected: number of threads\n");
        return EXIT_FAILURE;
    }

    char *end_ptr, *expected_end_ptr = argv[1] + strlen(argv[1]);
    long amountOfThreads = strtol(argv[1], &end_ptr, DECIMAL);
    if (end_ptr != expected_end_ptr) {
        fprintf(stderr, "Incorrect number format: %s\n", argv[1]);
        return EXIT_FAILURE;
    }
    if (amountOfThreads <= 0) {
        fprintf(stderr, "Expecting number >= 1\n");
        return EXIT_FAILURE;
    }

    double pi = 0.0;

    int error = calculatePi(&pi, amountOfThreads);
    if (error) {
        fprintf(stderr, "Could not calculate pi\n");
        return EXIT_FAILURE;
    }

    printf("Result: %lf\n", pi);

    return EXIT_SUCCESS;
}
