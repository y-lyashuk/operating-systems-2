#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#define NO_OF_THREADS 4

void* printLines(void* args) {
    if (args == NULL) {
        fprintf(stderr, "error: passing null-pointer as a function argument");
        return NULL;
    }

    char** lines = (char**) args;
    for (int i = 0; lines[i] != NULL; i++) {
        printf("%s\n", lines[i]);
    }

    return NULL;
}

int main(void) {
    pthread_t threads[NO_OF_THREADS];
    char** linesForThreads[NO_OF_THREADS] = {
            (char*[]) {"Thread 1 Line 1", "Thread 1 Line 2", NULL},
            (char*[]) {"Thread 2 Line 1", "Thread 2 Line 2", "Thread 2 Line 3", NULL},
            (char*[]) {"Thread 3 Line 1", "Thread 3 Line 2", "Thread 3 Line 3", "Thread 3 Line 4", NULL},
            (char*[]) {"Thread 4 Line 1", "Thread 4 Line 2", "Thread 4 Line 3", "Thread 4 Line 4", "Thread 4 Line 5", NULL}
    };

    int error, amountOfStartedThreads;
    for (amountOfStartedThreads = 0; amountOfStartedThreads < NO_OF_THREADS; amountOfStartedThreads++) {
        error = pthread_create(&threads[amountOfStartedThreads], NULL, printLines, linesForThreads[amountOfStartedThreads]);
        if (error) {
            fprintf(stderr, "pthread_create error: %s\n", strerror(error));
            break;
        }
    }

    for (int i = 0; i < amountOfStartedThreads; i++) {
        error = pthread_join(threads[i], NULL);
        if (error) {
            fprintf(stderr, "pthread_join error: %s\n", strerror(error));
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}