#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>

#define NUMBER_OF_LINES 10
#define AMOUNT_OF_MUTEXES 3

#define TRUE 1
#define FALSE 0

#define MUTEX_LOCKED_BY_CHILD 2
#define MUTEX_LOCKED_BY_PARENT 0

#define CHILD_FAILURE (void*)-1
#define CHILD_SUCCESS NULL

pthread_mutex_t mutexes[AMOUNT_OF_MUTEXES];
volatile sig_atomic_t parentAllowedToPrint = FALSE;

int printLines(int indexOfLockedMutex, char* message) {
    if (message == NULL) {
        fprintf(stderr, "error: null-pointer as an argument");
        pthread_mutex_unlock(&mutexes[indexOfLockedMutex]);
        return EXIT_FAILURE;
    }

    if (MUTEX_LOCKED_BY_CHILD == indexOfLockedMutex) {
        parentAllowedToPrint = TRUE;
    }

    int indexOfMutexToUnlock = indexOfLockedMutex, indexOfMutexToLock, error;
    for (int i = 0; i < NUMBER_OF_LINES; i++) {
        indexOfMutexToLock = (indexOfMutexToUnlock + 1) % AMOUNT_OF_MUTEXES;

        error = pthread_mutex_lock(&mutexes[indexOfMutexToLock]);
        if (error) {
            fprintf(stderr, "pthread_mutex_lock error: %s\n", strerror(error));
            pthread_mutex_unlock(&mutexes[indexOfMutexToUnlock]);
            return EXIT_FAILURE;
        }

        printf("%s: %d\n", message, i);

        pthread_mutex_unlock(&mutexes[indexOfMutexToUnlock]);
        indexOfMutexToUnlock = indexOfMutexToLock;
    }

    pthread_mutex_unlock(&mutexes[indexOfMutexToUnlock]);

    return EXIT_SUCCESS;
}

void* bodyOfChildThread(void* args) {
    int error = pthread_mutex_lock(&mutexes[MUTEX_LOCKED_BY_CHILD]);
    if (error) {
        fprintf(stderr, "pthread_mutex_lock error: %s\n", strerror(error));
        return CHILD_FAILURE;
    }

    error = printLines(MUTEX_LOCKED_BY_CHILD, "child");
    if (error) {
        return CHILD_FAILURE;
    }

    return CHILD_SUCCESS;
}

int destroyMutexes() {
    for (int i = 0; i < AMOUNT_OF_MUTEXES; i++) {
        int error = pthread_mutex_destroy(&mutexes[i]);
        if (error) {
            fprintf(stderr, "pthread_mutex_destroy error: %s\n", strerror(error));
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

int initMutexes() {
    pthread_mutexattr_t attributes;

    int error = pthread_mutexattr_init(&attributes);
    if (error) {
        fprintf(stderr, "pthread_mutexattr_init error: %s\n", strerror(error));
        return EXIT_FAILURE;
    }

    error = pthread_mutexattr_settype(&attributes, PTHREAD_MUTEX_ERRORCHECK);
    if (error) {
        fprintf(stderr, "pthread_mutexattr_settype error: %s\n", strerror(error));

        error = pthread_mutexattr_destroy(&attributes);
        if (error) {
            fprintf(stderr, "pthread_mutexattr_destroy error: %s\n", strerror(error));
        }

        return EXIT_FAILURE;
    }

    int mutexCount, returnValue = EXIT_SUCCESS;
    for (mutexCount = 0; mutexCount < AMOUNT_OF_MUTEXES; mutexCount++) {
        error = pthread_mutex_init(&mutexes[mutexCount], &attributes);
        if (error) {
            fprintf(stderr, "pthread_mutex_init error: %s\n", strerror(error));

            returnValue = EXIT_FAILURE;
            break;
        }
    }

    if (mutexCount != AMOUNT_OF_MUTEXES) {
        for (int i = 0; i < mutexCount; i++) {
            error = pthread_mutex_destroy(&mutexes[i]);
            if (error) {
                fprintf(stderr, "pthread_mutex_destroy error: %s\n", strerror(error));
            }
        }
    }

    error = pthread_mutexattr_destroy(&attributes);
    if (error) {
        fprintf(stderr, "pthread_mutexattr_destroy error: %s\n", strerror(error));
        destroyMutexes();
        returnValue = EXIT_FAILURE;
    }

    return returnValue;
}

int main(int argc, char **argv) {
    pthread_t thread;
    int returnValue = EXIT_SUCCESS;

    int error = initMutexes();
    if (error) {
        return EXIT_FAILURE;
    }

    error = pthread_mutex_lock(&mutexes[MUTEX_LOCKED_BY_PARENT]);
    if (error) {
        fprintf(stderr, "pthread_mutex_lock error: %s\n", strerror(error));
        destroyMutexes();
        return EXIT_FAILURE;
    }

    error = pthread_create(&thread, NULL, bodyOfChildThread, NULL);
    if (error) {
        fprintf(stderr, "pthread_create error: %s\n", strerror(error));
        destroyMutexes();
        return EXIT_FAILURE;
    }

    while (parentAllowedToPrint == FALSE) {
        sleep(1);
    }

    error = printLines(MUTEX_LOCKED_BY_PARENT, "parent");
    if (error) {
        returnValue = EXIT_FAILURE;
    }

    void* childResult;
    error = pthread_join(thread, &childResult);
    if (error) {
        fprintf(stderr, "pthread_join error: %s\n", strerror(error));
        destroyMutexes();
        return EXIT_FAILURE;
    }
    if (CHILD_FAILURE == childResult) {
        destroyMutexes();
        return EXIT_FAILURE;
    }

    error = destroyMutexes();
    if (error) {
        return EXIT_FAILURE;
    }

    return returnValue;
}
